# README #

W ramach zaj�� studenci s� zobowi�zani (w ramach pracy w grupach 3-4 osobowych) do przygotowania strony internetowej w jednym z wybranych �rodowisk back-end. Na zaj�ciach w styczniu ka�da z grup musi zaprezentowa� sw�j projekt lub czego i dlaczego nie uda�o si� wykona�. Ka�da z grup przygotowuje projekt w innym �rodowisku:

1. Python (Django, web2py, Pyramid, Flask)

2. Node.js (R + Shiny)

3. PHP

4. .net

5. Ruby

6. Java

Korzystaj�c z nast�puj�cych bibliotek, mi�dzy innymi (mo�na dodatkowo u�y� innych):

1. D3.js

2. Three.js

3. Plotly

4. Seaborn 

5. ...

Za�o�enia do aplikacji:

1. Interfejs WWW

2. Opcja logowania do strony

3. Dla u�ytkownika zalogowanego:

   - wczytywanie przyk�adowego pliku

   - na podstawie wczytanego pliku wygenerowa� wykres interaktywny/ obliczenia (r�wnie� 3D - scatterplot 3D, mo�liwo�� interakcji*, obrot�w)

   - przechowywanie pliku, wyniku, eksport grafiki do pliku graficznego

4. Dla u�ytkownika niezalogowanego:

   - podgl�d przyk�adowego pliku

   - interaktywna wizualizacja danych

   - brak mo�liwo�ci przechowywania wyniku

5. Umieszczenie na stronie formularza, zawieraj�cego:

   - pole tekstowe

   - pola wyboru jedno i wielokrotnego (checkbox i radio button)

   - przycisk wys�ania i anulowania

6. Strona do przegl�dania przes�anych przez formularz danych

* - na przyk�adzie pliku iris mo�liwo�� w��czania/wy��czania wizualizacji grup (np. za pomoc� check box)